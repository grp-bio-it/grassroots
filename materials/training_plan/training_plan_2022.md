# Training plan 2022

The "lead" is to be intended as the reference contact within the Bio-IT project, although some course are mainly organised by collaborators.

| Tag        | Meaning                 |
| ---------- | ----------------------- |
| :star:     | Confirmed with trainers |
| :calendar: | Confirmed dates         |

- 2022.01.31 - 2022.02.04 :star: :calendar: [**Data Carpentry**](https://git.embl.de/grp-bio-it-internal/internal-tasks-and-documentation/-/issues/124)
    - Lead: Renato
    - Team: Fotis Psomopoulos, Gaurav Diwan, Supriya Khedkar, Eleonora Mastrorilli, Luo Yan Yong (LuoYan), Hugo Tavares, Lisanna, Florian Huber, Ece Kartal

- 2022.01.28 :star: **Opening seminar: Workflow management systems** 
    - Lead: Lisanna
    - Team: Jean-Karim Heriche, Charles Girardot, Anthony Fullam, Matthias Monfort, Thomas Schwarzl, Christian Arnold, Christian Schudoma, Jelle Scholtalbers, Dominik Kutra, Jurij Pecar, Sudeep Sahadevan, Josep Moscardo, Christian Tischer, Renato 

- 2022.02.14 - 2022.02.16 :star: :calendar: [**Workflow management systems workshop**](https://docs.google.com/document/d/1sAYEgg4-87DwfY8mi_xxQ0QqYrKhMjbReTcukuLslbM)
    - Lead: Lisanna
    - Team: _see below_ 

- 2022.02.28 - 2022.03.04 **Science FAIR** [Open Science training course](https://pad.bio-it.embl.de/lSVF8GzYQdGheIo-GeGDNQ), also related [Open Science training materials](https://pad.bio-it.embl.de/14iY8iqoTlCKl9qqFImrYQ) - targeted internally EMBL
    - Lead: Lisanna
    - Introduction from [CyVerse](https://learning.cyverse.org/projects/cyverse-foss/en/latest/index.html)
    - Spreadsheets from [Life Science Workshop](https://carpentries-incubator.github.io/life-sciences-workshop/)
    - Materials from predoc course 
    - [FAIR workshop](https://events.tib.eu/fair-data-software/programme/)
    - [Old materials from data management course](https://git.embl.de/grp-bio-it-workshops/data-management-course) (5 years ago)

- 2022.02.28 - 2022.03.02 :star: :calendar: **Deep Learning course** - 3 afternoons 
    - Lead: Lisanna 
    - Team: Renato, Lazlo, Dominik

- 2022.03 [Scientific Design course/club (with Inkscape)](https://pad.bio-it.embl.de/7o6fxlk8ToKhoxuUVVW88g) - 1 day - in person
    - Lead: Renato
    - Team: Thea Van Rossum?

- 2022.03 **HPC course** (requires revision of materials) 
    - Lead: Renato
    - Team: Lisanna

- 2022.04 **Python beginners** - co-organised with EMBL Rome? 
    - Lead: Lisanna
    - Team: Renato, Mattia Forneris?, Nicolas Decostes? Francesco Tabaro?

- 2022.04 **R beginners (Tidyverse)** 
    - Lead: Renato
    - Team: Lisanna

- 2022.05 **[Protein bioinformatics course](https://git.embl.de/grp-bio-it-workshops/introduction-to-protein-bioinformatics-tools-2020)** 
    - Lead: Renato 
    - Team: Gibson group, Lisanna

- 2022.05 **Ontologies - Statistics, Biases, Tools, Networks, and Interpretation** 
    - Lead: Lisanna 
    - Team: Balint Meszaros

- 2022.06 **CodeRefinery Intermediate/Advanced Python** - [training events call](https://github.com/coderefinery/coderefinery.org/blob/6f0afb3/content/about/community-call.md) - [training hubs](https://coderefinery.org/about/hubs/) or [Bio-IT **Python intermediate**](https://grp-bio-it-workshops.embl-community.io/intermediate-python/) 
    - Lead: Renato
    - Team: Lisanna, Mattia Forneris?

- 2022.06 **Exploratory data analysis / Metagenomics** (Introduction to Dimensionality Reduction and Clustering) 
    - Lead: Georg
    - Team: Zeller team?

- 2022.07 [Structural Biology and Dynamics course](https://pad.bio-it.embl.de/6ruHqjKCTPS-ao7nHb8mAQ) - possibly external, involve the Data Science Theory group 
    - Lead: Lisanna
    - Team: Kasifh, Jan Kosinski?

- 2022.07 **Train the Trainers** (Carpentry or ELIXIR)
    - Lead: Lisanna
    - Team: Renato

- 2022.09 **Introduction to Python and Pandas** (Software Carpentry based) - to be confirmed 
    - Lead: Renato
    - Team: Lisanna, Mattia Forneris?

- 2022.09 **R beginners (without Tidyverse)** 
    - Lead: Renato

- 2022.09 [**Julia course**](https://pad.bio-it.embl.de/H7xuxofNR9OAPKr9g4ldjQ) - low priority, Python and R first 
    - Lead: Renato
    - Team: Lisanna

- 2022.10 **Cloud and container computing** / [ISB-CGC course](https://pad.bio-it.embl.de/xQgbB5TyQKW2bVQgP4M-sA)?
    - Lead: Lisanna
    - Team: Georg

- 2022.10 **Building websites with GitLab** 
    - Lead: Lisanna
    - Team: Renato

- 2022.11 **Workflow management systems** 
    - Lead: Lisanna
    - Team: Renato

- 2022.11 **Git Week** 
    - Lead: Renato
    - Team: Lisanna