The Grassroots initiative is dedicated to providing a collaborative
and supportive environment in the computational biology community
and beyond at EMBL. By participating in this community, you also 
accept to abide by EMBL's Code of Conduct. Continue reading for 
more details.

## EMBL's Code of Conduct

Find the original text in the [EMBL's Conference and Courses page](https://www.embl.de/training/events/info_participants/codeofconduct/#:~:text=EMBL%20is%20dedicated%20to%20providing,thereof)%20or%20manner%20of%20articulation.).
This version has been slightly edited to fit the purposes of the Grassroots project.

- EMBL is dedicated to providing a harassment-free collaborating experience for everyone, regardless of gender, gender 
identity and expression, age, sexual orientation, disability, physical appearance, body size, ethnicity, religion 
(or lack thereof) or manner of articulation. Harassment of participants in any form is not tolerated by EMBL.

- We recognise the inherent worth of every person by being respectful to them. All forms of communication should be 
appropriate for a professional audience including people of different backgrounds. 

- Participants violating these terms and conditions or the EMBL Code of Conduct may be asked to leave the 
communication channel and may be banned from participating to grassroots consulting again.

- Participants shall conduct themselves in such a manner so as not to jeopardise EMBL’s reputation during their 
participation in Bio-IT grassroots project.

- Participation is governed by EMBL Code of Conduct as well as applicable internal and national legislation.

- Please contact us at [bio-it@embl.de](mailto:bio-it@embl.de) if you have questions or experience any violation of 
our code of conduct. This email address is accessible to Lisanna Paladin and Renato Alves - Bio-IT project managers.
To report an issue involving one of them, or if you prefer to address them individually, please email 
[lisanna.paladin@embl.de](mailto:lisanna.paladin@embl.de) or [renato-alves@embl.de](mailto:renato-alves@embl.de).

Also relevant,
- EMBL provides an extended [guide on how to deal with problems at EMBL](https://www.embl.org/documents/wp-content/uploads/2020/07/brochure_how-to-deal-with-problems.pdf).
- EMBL [subscribed](http://www.embl-hamburg.de/aboutus/communication_outreach/media_relations/2006/061214_brussels/) the [Code of Conduct for the Recruitment of Researchers](https://euraxess.ec.europa.eu/jobs/charter/code).

