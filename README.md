# Grassroots: Nurturing the EMBL Bio-IT Community

## Project mission and summary

The **Bio-IT Grassroots** initiative aims at bringing together members of EMBL's bio-computational community through reciprocal assistance on a wide range of topics. It serves as a platform to **support internal collaboration and consultation at EMBL**. 

The activity was initiated in 2019 and it currently includes a table of EMBL **personnel available for consultation, respective topics of expertise and preferred communication channels**. The [grassroots webpage](https://bio-it.embl.de/grassroots-consulting/) also includes instructions on how to get involved, and states that grassroot consultants are indicated by the grass 🌱 icon in [EMBL chat](https://chat.embl.org/) and [EMBL GitLab](https://git.embl.de/). 

The purpose of this project is to reactivate and improve the Bio-IT Grassroots initiative, by updating the relative data, advertising it, recruiting new members and acknowledge their contribution. The possibility of organising recurring or special events with these aims will be explored. Significant milestones will be established as control-points of the project progression. This project progression is monitored and supported through participation to the [forth cohort of the Open Life Sciences initiative](https://openlifesci.org/ols-4). OLS-related documents are linked at [#1](https://git.embl.de/grp-bio-it/grassroots/-/issues/1).

**Project roadmap** available at [#2](https://git.embl.de/grp-bio-it/grassroots/-/issues/2).

## Project Canvas

![canvas](graphics_visual-identity/canvas.png)

## Participation guidelines

### Code of conduct

Grassroots participants adhere to a [code of conduct](CODE_OF_CONDUCT.md). By participating as a contributor of this 
repository, as a consultant or as a participant requesting a consultation session you are expected to 
uphold this code. Please report unacceptable behavior to [bio-it@embl.de](mailto:bio-it@embl.de).

### License

All content in this repository is released under the [Created Commons Attribution CC BY](LICENSE.md).

### Contribution guidelines

We want to ensure that everyone feels welcome, included and supported to participate in the community.
To this aim, all types of contributions to this repository are encouraged (additions to the content, edits and issue 
opening). You are most welcomed as well to [contact us](mailto:bio-it@embl.de) to chat about this project and propose 
your ideas, or to comment the [issue #7](https://git.embl.de/grp-bio-it/grassroots/-/issues/7) "Feedback from collaborators".

**Would you like to help?** Please **review the list of skills** available in 
[materials](https://git.embl.de/grp-bio-it/grassroots/-/tree/main/materials). Check the related issue 
([#3](https://git.embl.de/grp-bio-it/grassroots/-/issues/3)) for more details on the purpose of this document.

## Useful links

- [Bio-IT website](https://bio-it.embl.de/)
- [Grassroots main page](https://bio-it.embl.de/grassroots-consulting/)
