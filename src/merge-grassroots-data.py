import os
import pandas as pd

# get absolute path
project_path = os.path.dirname(__file__).strip('src')
data_path = project_path + 'data/'

output = project_path + 'materials/output.csv'

old_data = pd.read_csv(data_path + 'old-data-wordpress.csv')
new_data = pd.read_csv(data_path + 'new-data-survey.csv')

expertise_keywords = [
    'Using specialised research software',
    'Exploratory data analysis and visualisation',
    'Programming languages',
    'Statistics and machine learning',
    'Image analysis',
    'Cluster computing',
    'Command-line computing',
    'Computational workflow management',
    'Cloud computing',
    'GPU computing',
    'Data management and curation',
    'Software project management',
    'Biological networks analysis',
    'Biological modelling',
    'Text mining',
    'Benchmarking bioinformatics tools',
    'Web technologies',
    'System administration',
    'Transcriptomics',
    'Genomics and comparative genomics',
    'Proteomics and protein analysis',
    'Bioimaging',
    'Cancer genomics and personalised medicine',
    'Metagenomics and other meta-omics',
    'Structural biology',
    'Metabolomics',
    'Neurobiology and neuroinformatics'
]

# add column for sorting
old_data['sorting'] = 0
# update data
for index, row in new_data.iterrows():
    # print(row.keys())
    name = row['Please add your Name, Surname'].replace(',', '')
    topics = ', '.join([e for e in expertise_keywords if row[e] == 'Yes'])
    match = old_data['Name & contact info'].str.contains(name)
    if match.any():
        # modify existing entry in the table
        old_data.loc[match, 'Name & contact info'] = old_data[match]['Name & contact info'] \
                            + '</br><span class="badge badge-info">Updated</span>'  # add updated badge
        old_data.loc[match, 'Topics'] = topics + '</br></br>' + old_data[match]['Topics']
        old_data.loc[match, 'sorting'] = 1
    else:
        # add this entry to the table
        old_data = old_data.append({
            'Name & contact info': name + '</br><span class="badge badge-info">Updated</span>',
            'Topics': topics,
            'Preferred communication channel</br>(by order of preference)': '-',
            'sorting': 1
        }, ignore_index=True)

# sort data by alphabetical order
old_data = old_data.sort_values('sorting', ascending=False)
old_data = old_data.drop('sorting', 1)

# rename columns
old_data = old_data.rename(columns={
    "Preferred communication channel</br>(by order of preference)":
        'Communication channels</br><span class="badge badge-info">In order of preference</span>'
})
old_data = old_data.rename(columns={
    "Name & contact info":
        'Name'
})
old_data = old_data.rename(columns={
    "Topics":
        'Expertise'
})

old_data.to_csv(output, index=False)